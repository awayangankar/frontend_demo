import React from 'react';
import 'babel-polyfill'
import PeopleList from '../../components/PeopleList/PeopleList.jsx'
import ContactUs from '../../components/ContactUs/ContactUs.jsx'
import Container from '../../components/Container/Container.jsx'
import Slider from '../../components/Slider/Slider.jsx'
import Card from '../../components/Card/Card.jsx'
import banner from '../../assets/banner/banner.png';
import content_1 from '../../assets/content/content-001.png';
import content_2 from '../../assets/content/content-002.png';

import gaetan from '../../assets/people/gaetan-houssin.jpg';

import apple from '../../assets/partners/apple.png';
import airbnb from '../../assets/partners/airbnb.png';
import google from '../../assets/partners/google.png';
import nvidia from '../../assets/partners/nvidia.png';
import tesla from '../../assets/partners/tesla.png';
import samsung from '../../assets/partners/samsung.png';
import facebook from '../../assets/partners/facebook.png';
import microsoft from '../../assets/partners/microsoft.png';

import './Home.css'

const Home = () => {
    const BergerMenu = [
        { menu: "Home" },
        { menu: "AboutUs" },
        { menu: "Pricing" },
    ]

    return (
        <div className='home-wrapper'>
            <div className='banner-img'>
                <div>
                    <img className='banner-right' src={banner}></img>
                </div>

            </div>
            <div className='container-full'>
                <Container content_main={'content_1_main'} content_img={content_1} content_1={'content_1'} />
            </div>
            <div className='container-full'>
                <Container content_main={'content_2_main'} content_img={content_2} content_1={'content_2'} />
            </div>
            <div className='container-full'>
                <div className='container-max people-about-services'>
                    <h2>
                        <span className='heart'>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clipRule={"evenodd"} />
                            </svg>
                        </span>
                        What other people say about our services</h2>
                    <div className='card-carousel'>
                        <Card img={gaetan} label={"ABCD"} Position={"Ceo of ABCD"} Content={"Lorem Ipsum is simply dummy text of the printing and typesetting industry."}></Card>
                        <Card img={gaetan} label={"ABCD"} Position={"Ceo of ABCD"} Content={"Lorem Ipsum is simply dummy text of the printing and typesetting industry."}></Card>
                    </div>
                </div>
            </div>
            <div className='container-full'>
                <PeopleList />
            </div>
            <div className='container-full'>
                <div className='container-max'>
                    <div className='have-work-with'>
                        <h2>We have worked with</h2>
                        <div>
                            <img src={apple}></img>
                            <img src={airbnb}></img>
                            <img src={google}></img>
                            <img src={nvidia}></img>
                            <img src={tesla}></img>
                            <img src={samsung}></img>
                            <img src={facebook}></img>
                            <img src={microsoft}></img>
                        </div>
                    </div>
                </div>
            </div>
            <div className='container-full'>
                <div className='container-max'>
                    <ContactUs />
                </div>
            </div>
            {window.screen.availWidth <= 1023 ? <Slider
                // customCrossIcon={""}
                menu={BergerMenu}
            // onMenuClick={this.onBergerMenuClick}
            // isOpenMenu={this.state.isOpenMenuBeforeLogin}
            /> : null}
        </div>
    )
}

export default Home;