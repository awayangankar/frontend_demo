import { v4 as uuidv4 } from 'uuid';
import gaetan from '../assets/people/gaetan-houssin.jpg';
import nicolas from '../assets/people/nicolas-lebarreau.jpg';
import jerome from '../assets/people/jerome-mahuet.jpg';
import manuela from '../assets/people/manuela-faveri.jpg';
import darlene from '../assets/people/darlene-chabrat.jpg';
import romane from '../assets/people/romane-regad.jpg';
import tania from '../assets/people/tania-ferreira.jpg';

const idOne = uuidv4();
const idTwo = uuidv4();
const idThree = uuidv4();
const idFour = uuidv4();
const idFive = uuidv4();
const idSix = uuidv4();
const idSeven = uuidv4();

const users = [
    { ID: idOne, Name: "Gaetan Houssin", Position: "CEO", Description: "", Picture: gaetan },
    { ID: idTwo, Name: "Nicolas Lebarreau", Position: "UI/UX Designer", Description: "", Picture: nicolas },
    { ID: idThree, Name: "Jerome Mahuet", Position: "Full Stack Developer", Description: "", Picture: jerome },
    { ID: idFour, Name: "Manuela Faveri", Position: "Marketing", Description: "", Picture: manuela },
    { ID: idFive, Name: "Darlene Chabrat", Position: "Customer Support", Description: "", Picture: darlene },
    { ID: idSix, Name: "Romane Regad", Position: "Jr.UI/UX Designer", Description: "", Picture: romane },
    { ID: idSeven, Name: "Tania Ferreira", Position: "Business Analyst", Description: "", Picture: tania },
]

export const getUsers = () =>
    new Promise((resolve, reject) => {
        if (!users) {
            return setTimeout(
                () => reject(new Error('Users not found')),
                250
            );
        }
        setTimeout(() => resolve(Object.values(users)), 250);
    });

export const addUpdateUser = (ID, Person, usersData) =>
    new Promise((resolve, reject) => {
        var newList = usersData;
        if (ID !== 0) {
            newList = usersData.map((item) => {
                if (item.ID === ID) {
                    const updatedItem = {
                        ...item,
                        Name: Person.Name,
                        Position: Person.Position,
                        Description: Person.Description
                    };
                    return updatedItem;
                }
                return item;
            });
        }
        else {
            newList.push({ ID: uuidv4(), Name: Person.Name, Position: Person.Position, Description: Person.Description, Picture: gaetan })
        }

        return setTimeout(() => resolve(Object.values(newList)), 250);
    });

export const deleteUser = (ID) =>
    new Promise((resolve, reject) => {
        const { [ID]: user, ...rest } = users;

        if (!user) {
            return setTimeout(
                () => reject(new Error('User not found')),
                250
            );
        }
        users = { ...rest };
        return setTimeout(() => resolve(true), 250);
    });

