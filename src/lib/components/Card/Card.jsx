import React from 'react';
import './Card.css'

const Card = (props) => {
    return (
        <div className='card'>
            <div className='card-header'>
                <span className='card-user-img'><img src={props.img} /></span>
                <span className='card-header-text'>
                    <label><strong>{props.label}</strong></label>
                    <label>{props.Position}</label>
                </span>
            </div>
            <div className="card-content">
                {props.Content}
            </div>
        </div>
    );
}

export default Card