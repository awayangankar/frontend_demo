import React, { useState, useEffect } from 'react';
import ModalPopup from "../ModalPopup/ModalPopup.jsx"
import { FormErrors } from '../../utility/FormErrors';
import { getUsers, addUpdateUser } from '../../api/user.js'

import './PeopleList.css'

const PeopleList = (props) => {
    const [isOpen, setPopup] = useState(false)
    const [formValid, setFormValid] = useState({ isNameValid: false, isPositionValid: false, isDescriptionValid: false, isFormValid: false })
    const [Person, setPerson] = useState({ ID: 0, Name: '', Position: '', Description: '' })
    const [formErrors, setFormErrors] = useState({ Name: '', Position: '', Description: '' })
    const [users, setUsers] = useState(null);

    const displayPeople = (ID) => {
        setPopup(true)
        if (ID === 0) {
            setPerson({ ID: 0, Name: '', Position: '', Description: '' })
            setFormValid({ isNameValid: false, isPositionValid: false, isDescriptionValid: false, isFormValid: false })
        }
        setFormErrors({ Name: '', Position: '', Description: '' })
    }

    const handleUserInput = (e) => {
        const { name, value } = e.target;

        setPerson(prevFormValues => {
            return {
                ...prevFormValues,
                [name]: value
            }
        }, validateField(name, value))
    }

    const validateField = (fieldName, value) => {
        let { Name, Position, Description } = formErrors;
        let { isNameValid, isPositionValid, isDescriptionValid, isFormValid } = formValid;

        switch (fieldName) {
            case 'Name':
                isNameValid = value.length > 0;
                Name = isNameValid ? '' : ' is empty';
                break;
            case 'Description':
                isDescriptionValid = value.length > 0;
                Description = isDescriptionValid ? '' : ' is empty';
                break;
            case 'Position':
                isPositionValid = value.length > 0;
                Position = isPositionValid ? '' : ' is empty';
                break;
            default:
                break;
        }

        setFormErrors({ Name, Position, Description })
        setFormValid({ isNameValid, isPositionValid, isDescriptionValid, isFormValid })
        validateForm()
    }

    const validateForm = () => {
        setFormValid(prevFormValid => {
            return {
                ...prevFormValid,
                isFormValid: prevFormValid.isNameValid && prevFormValid.isPositionValid && prevFormValid.isDescriptionValid
            }
        })
    }

    const addEditData = async (ID) => {
        setPopup(false)
        try {
            const result = await addUpdateUser(ID, { Name: Person.Name, Position: Person.Position, Description: Person.Description }, users);
            setUsers(result);
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        const doGetUsers = async () => {
            try {
                const result = await getUsers();
                setUsers(result);
            } catch (error) {
                console.log(error);
            }
        };
        doGetUsers();
    }, []);

    return (
        <div className='container-max people-list'>
            <h2>
                <span className='smiley'>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z" clipRule={"evenodd"} />
                    </svg>
                </span>
                Our important people is listed here</h2>
            <div className='people-list-add-edit'>
                <button onClick={() => displayPeople(1)}>Edit</button>
                <button onClick={() => displayPeople(0)}>Add</button>
            </div>
            <div className='people-card-flex'>
                {users && users.map((record, key) => {
                    return (
                        <div className='people-card' key={key}>
                            <span className={'edit-checkbox'}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="#2b61b4">
                                    <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
                                </svg>
                                <input type={"checkbox"} value={record.ID} onChange={() => setPerson({ ID: record.ID, Name: record.Name, Position: record.Position, Description: record.Description })} checked={Person.ID === record.ID}></input>
                            </span>

                            <img src={record.Picture}></img>
                            <label><strong>{record.Name}</strong></label>
                            <label>{record.Position}</label>
                        </div>
                    )
                }
                )}
            </div>
            {isOpen ? <ModalPopup isPopup={true} onClosePopup={() => setPopup(false)}>
                <div className='form w-100'>
                    <div className='form-input'><label>Name</label> <input type="text" name="Name" value={Person.Name} onChange={(e) => handleUserInput(e)}></input></div>
                    <div className="panel panel-default">
                        <FormErrors formErrors={formErrors} fieldKey={"Name"} />
                    </div>
                    <div className='form-input'><label>Position</label> <input type="text" name="Position" value={Person.Position} onChange={(e) => handleUserInput(e)}></input></div>
                    <div className="panel panel-default">
                        <FormErrors formErrors={formErrors} fieldKey={"Position"} />
                    </div>
                    <div className='form-input'><label>Description</label>
                        <textarea value={Person.Description} name="Description" onChange={(e) => handleUserInput(e)} /></div>
                    <div className="panel panel-default">
                        <FormErrors formErrors={formErrors} fieldKey={"Description"} />
                    </div>
                    <div className='form-button'><button className='cancel' onClick={() => setPopup(false)}>Cancel</button>
                        <button type="submit" className="btn btn-primary" disabled={!formValid.isFormValid}>Submit</button>
                    </div>
                </div>
            </ModalPopup> : null}
        </div>
    );
}

export default PeopleList