import React from 'react';
import { slide as Menu } from "react-burger-menu";
import './Slider.css';
import logo from '../../assets/header/logo.png';
const Slider = (props) => {
    return (
        <div>
            <Menu
                {...props}
                className='black'
                isOpen={props.isOpenMenu}>
                    <div className='menu-logo'>
                        <img src={logo} alt='logo'/>
                    </div>
                <ul>
                    {
                        props.menu.map((element, index) => {
                            return (
                                <li className={element.isLine ? '' : ''} onClick={() => props.onMenuClick(element, index)} key={index}>
                                    <span className={element.isImageIconRight ? 'menu-image-right' : 'menu-image-left'}>
                                        </span><span className='menu-nav-title'>{element.menu}
                                        {element.count && element.count !== 0 && <span className='mobile-dropdown-trip-count'>{element.count}</span>}
                                    </span>
                                </li>
                            );
                        })
                    }
                </ul>
            </Menu>
        </div>
    );
}
export default Slider;

