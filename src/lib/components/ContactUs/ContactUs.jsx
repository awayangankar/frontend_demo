import React, { useState, useEffect } from 'react';
import { FormErrors } from '../../utility/FormErrors';
import './ContactUs.css'

const ContactUs = (props) => {
    const [formValid, setFormValid] = useState({ isNameValid: false, isEmailValid: false, isSubjectValid: false, isMessageValid: false, isFormValid: false })
    const [formValues, setFormValues] = useState({ name: '', email: '', subject: '', message: '' })
    const [formErrors, setFormErrors] = useState({ name: '', email: '', subject: '', message: '' })

    const handleUserInput = (e) => {
        const { name, value } = e.target;

        setFormValues(prevFormValues => {
            return {
                ...prevFormValues,
                [name]: value
            }
        }, validateField(name, value))
    }

    const validateField = (fieldName, value) => {
        let { name, email, subject, message } = formErrors;
        let { isNameValid, isEmailValid, isSubjectValid, isMessageValid, isFormValid } = formValid;

        switch (fieldName) {
            case 'name':
                isNameValid = value.length > 0;
                name = isNameValid ? '' : ' is empty';
                break;
            case 'email':
                isEmailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                email = isEmailValid ? '' : ' is invalid';
                break;
            case 'subject':
                isSubjectValid = value.length > 0;
                subject = isSubjectValid ? '' : ' is empty';
                break;
            case 'message':
                isMessageValid = value.length > 0;
                message = isMessageValid ? '' : ' is empty';
                break;
            default:
                break;
        }

        setFormErrors({ name, email, subject, message })
        setFormValid({ isNameValid, isEmailValid, isSubjectValid, isMessageValid, isFormValid })
        validateForm()
    }

    const validateForm = () => {
        setFormValid(prevFormValid => {
            return {
                ...prevFormValid,
                isFormValid: prevFormValid.isNameValid && prevFormValid.isEmailValid && prevFormValid.isSubjectValid && prevFormValid.isMessageValid
            }
        })
    }

    return (
        <div>
            <div className='contact-us'>
                <h2>Contact Us</h2>
                <div className='form-map'>
                    <div className='form'>
                        <div className="panel panel-default">
                            {/* <FormErrors formErrors={formErrors} /> */}
                        </div>
                        <div className='form-input'>
                            <label> Name </label>
                            <input placeholder='Enter your name' name="name" value={formValues.name} type="text" onChange={handleUserInput}></input>
                        </div>
                        <div className="panel panel-default">
                            <FormErrors formErrors={formErrors} fieldKey={"name"} />
                        </div>
                        <div className='form-input'>
                            <label> Email </label>
                            <input type="email" placeholder='Enter your Email' value={formValues.email} name="email" onChange={handleUserInput}></input>
                        </div>
                        <div className="panel panel-default">
                            <FormErrors formErrors={formErrors} fieldKey={"email"} />
                        </div>
                        <div className='form-input'>
                            <label> subject </label>
                            <input type="text" placeholder='Enter subject' value={formValues.subject} name="subject" onChange={handleUserInput}></input>
                        </div>
                        <div className="panel panel-default">
                            <FormErrors formErrors={formErrors} fieldKey={"subject"} />
                        </div>
                        <div className='form-input'>
                            <label> Message </label>
                            <textarea type="" placeholder='Enter message' value={formValues.message} name="message" onChange={handleUserInput}></textarea>
                        </div>
                        <div className="panel panel-default">
                                <FormErrors formErrors={formErrors} fieldKey={"message"} />
                            </div>
                        <button type="submit" className="btn btn-primary" disabled={!formValid.isFormValid}>Submit</button>
                    </div>
                    <div className='map'></div>
                </div>
            </div>
        </div>
    );
}

export default ContactUs