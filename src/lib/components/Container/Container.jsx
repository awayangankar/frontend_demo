import React from 'react';
import './Container.css'

const Container = (props) => {
    return (
        <div className={'container-max ' + props.content_main}>
            <div className={props.content_1+'_img'}>
                <img src={props.content_img}></img>
            </div>
            <div className={props.content_1}>
                <h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p>
            </div>
        </div>
    );
}

export default Container