import React from 'react';
import Header from './Header/Header.jsx';
import Footer from './Footer/Footer.jsx';

const Layout = (props) => {
    return (
        <div>
            <Header />
            <div className='main-content' id='main-content' style={{'height':'50%'}}>
                {props.children}
            </div>
            <Footer />
        </div>
    );
}

export default Layout