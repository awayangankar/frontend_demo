import React from 'react';
import footerLogo from '../../../assets/header/logo.png';
import './Footer.css'
const Footer = () => {

    return (
        <div>
            <div className='footer-wrapper' id='footer-id'>
                <footer>
                    <span>
                    <img src={footerLogo}></img>
                    <label><strong>Easy Work</strong></label>
                    </span>
                    <span>
                        <label><strong>Address</strong></label>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum velit, accusa</p>
                    </span>
                    <span>
                        <div>
                        <label><strong>Contact</strong></label>
                        <p>09-766-999-77</p>
                        </div>
                        <div>
                        <label><strong>Fax</strong></label>
                        <p>09-766-999-77</p>
                        </div>
                    </span>
                </footer>
            </div>
        </div>
    )
};

export default Footer
