import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../assets/header/logo.png';
import './Header.css'

const Header = () => {

    return (
        <div>
            <div className='header-wrapper' id='header-id'>
                <header>
                    <img src={logo}></img>
                    <div className='header-links'>
                        <div className='active'><Link to="/">Home</Link></div>
                        <div><Link to="/aboutus">About Us</Link></div>
                        <div><Link to="/aboutus">Service</Link></div>
                    </div>
                    <div className='search'>

                    </div>
                </header>
            </div>
        </div>
    )
};

export default Header
