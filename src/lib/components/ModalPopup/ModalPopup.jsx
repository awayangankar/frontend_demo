import React from 'react';
import "./ModalPopup.css"

const ModalPopup = (props) => {
    //let styles = styles //props ? props.modalCss : { "modal_container": "" };
    // console.log("props.isPopup", props.isPopup)
    return (
        <>
            {props.isPopup === true ? (
                <div className={"modal_container"}>
                    <div className={"modal_background"}></div>
                    <div className={"modal_overflow"}>
                        {props.isMobile === true ? (
                            <div
                                className={
                                    "modal_content" + " " + "mobile_modal"
                                }
                            >
                                {props.children}
                            </div>
                        ) : (
                            <div className={"modal_content"}>
                                {props.children}
                            </div>
                        )}
                    </div>
                </div>
            ) : null}
        </>
    )
}

export default ModalPopup;