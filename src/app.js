import React from 'react'
import {
    Route,
    BrowserRouter as Router,
    Routes
} from 'react-router-dom';

import { asyncComponent } from 'react-async-component';

const Layout = asyncComponent({
    resolve: () => import('./lib/components/Layout/Layout.jsx')
});
const Home = asyncComponent({
    resolve: () => import('./lib/pages/Home/Home.jsx'),
    LoadingComponent: () => null
});
const AboutUs = asyncComponent({
    resolve: () => import('./lib/pages/AboutUs/AboutUs.jsx')
});
export default () => {
    return (
        <Router>
            <Layout>
                <Routes>
                    <Route exact path="/" element={<Home/>}/>
                    <Route path='/aboutus' element={<AboutUs/>}/>
                </Routes>
            </Layout>
        </Router>
    )
}
